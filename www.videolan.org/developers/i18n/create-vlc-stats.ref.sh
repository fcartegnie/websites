#!/bin/sh
#
# This scripts creates a ref file to use with vlc website to report i18n statistics
# put it in vlc checkout's po/ directory and run to produce a file with output
#
set -x

OUTPUT="./vlc-stats.ref"
umask 022
export LC_ALL=C

truncate -s 0 $OUTPUT

for pofile in `ls *.po`; do
  tmp=`'msgfmt' --statistics -o /dev/null $pofile 2>&1`
  tr=`echo $tmp|awk '{print $1}'`
  ut=`echo $tmp|awk '{print $7}'`
  case $tr in
    "")
        tr=0
        ;;
  esac
  case $ut in
    "")
        ut=`echo $tmp|awk '{print $4}'`
        fu=0
        ;;
  esac
  case $ut in
    "")
        ut=0
        ;;
  esac
  case `echo $tmp|awk '{print $4}'` in
    "")
        fu=0
        ;;
  esac
  case `echo $tmp|awk '{print $5}'` in
    "fuzzy")
        fu=`echo $tmp|awk '{print $4}'`
        ;;
  esac
  echo $pofile" "$tr" "$fu" "$ut >> $OUTPUT
done
