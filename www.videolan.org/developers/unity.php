<?php
    $title = "VLC for Unity";
    $lang = "en";
    $new_design = true;
    $body_color = "red";
    require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<div class="container">
    <h1 class="bigtitle">VLC for Unity</h1>

    <p><code>vlc-unity</code> is an integration of the LibVLC engine with the Unity 3D game engine.</p>

    <img style="width:100%" src="//images.videolan.org/images/vlc-unity.png" alt="VLC for Unity"/>
    <h2>Get vlc-unity</h2>
    <p><code>vlc-unity</code> is available through <a href="https://wiki.videolan.org/git">git</a> at:</p>
    <pre><code>git clone https://code.videolan.org/videolan/vlc-unity</code></pre>

    <p>The source code can browsed at <a href="https://code.videolan.org/videolan/vlc-unity">code.videolan.org</a>.</p>
    <p>Contributions can be sent as merge requests in our <a href="https://code.videolan.org/videolan/vlc-unity">gitlab repository</a>.</p>
</div>
<?php footer(); ?>
