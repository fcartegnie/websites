<?php
   $title = "Video Dev Days 2023, September 23-24, 2023";
   $body_color = "orange";

   $new_design = true;
   $show_vdd_banner = false;
   $lang = "en";
   $menu = array( "videolan", "events" );

   $rev = time();

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/videolan/events/vdd23/style.css?$rev");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

  <div class="sponsor-box-2 d-xs-none d-md-block">
    <h4>Sponsors</h4>
    <a href="https://videolabs.io/" target="_blank">
        <?php image( 'events/vdd19/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo'); ?>
    </a>
  </div>
  <header class="header-bg">
      <div class="container">
        <div class="row col-lg-10 col-lg-offset-1">
          <div class="col-sm-12 col-md-4">
            <img class="img-fluid center-block" style="max-width:100%; height:auto; border-radius: 4%;" src="/images/events/vdd23/vdd23.png" alt="vdd logo">
          </div>
          <div class="col-sm-12 col-md-8 text-center align-middle">
            <h1 id="videodevdays">Video Dev Days 2023</h1>
            <h3>The Open Multimedia Conference that frees the cone in you!</h3>
            <h4>23 - 24 September, 2023, Dublin</h4>
          </div>
        </div>
      </div>
   </header>

<section id="overview">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 col-lg-offset-1 text-center">
        <h2 class="uppercase">
			 About
          <span class="spacer-inline"></span>
        </h2>

			<p><a href="/videolan/">VideoLAN non-profit organization</a> is pleased to welcome you to the next multimedia open-source event in Dublin!</p>
			<p>In the upcoming edition, individuals from the VideoLAN and open-source multimedia communities will converge in <strong>Dublin</strong> to discuss and collaborate on shaping the future of the open-source multimedia.</p>
			<p>This technical conference will concentrate on low-level multimedia elements, encompassing codecs and their implementations like <a href="https://www.videolan.org/developers/x264.html">x264</a> or <a href="https://code.videolan.org/videolan/dav1d">dav1d</a>, frameworks like FFmpeg or LibVLC, and lots of other exciting projects in the multimedia world.</p>

        <p>This is a <strong>technical</strong> conference, focused on low-level multimedia, like codecs and their implementations like <a href="https://www.videolan.org/developers/x264.html">x264</a> or <a href="https://code.videolan.org/videolan/dav1d">dav1d</a>, frameworks like FFmpeg or Gstreamer or playback libraries like libVLC.</p>
        
		<div>
		  <div class="row">
          <div class="col-md-6">
            <div class="text-box" id="when-box">
              <h4 class="text-box-title">When ?</h4>
              <p class="text-box-content">22 - 24 September 2023</p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box" id="where-box">
              <h4 class="text-box-title">Where ?</h4>
              <p class="text-box-content">Dublin, Ireland</p>
            </div>
          </div>
        </div>

		  <div class="row">
          <div class="col-md-6">
            <div class="text-box" id="who-come-box">
              <h4 class="text-box-title">For ?</h4>
              <p class="text-box-content">
        				<strong>Anyone</strong> who cares about open source multimedia technologies and development.
				  </p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="text-box" id="partners-box">
              <h4 class="text-box-title">From ?</h4>
              <p class="text-box-content">VideoLAN. But you are very welcome to co-sponsor the event.</p>
            </div>
          </div>
        </div>
 		
		</div>

      </div>
    </div>
  </div>
</section>

<section id="schedule">
  <div class="container">
    <div class="row">
      <div class="text-center">
        <h2 class="uppercase">Schedule</h2>
        <hr class="spacer-2">
      </div>
      <div class="col-lg-10 col-lg-offset-1">
      <!-- Nav tabs -->

      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation"><a href="#friday" aria-controls="friday" role="tab" data-toggle="tab">Friday 22</a></li>
        <li role="presentation" class="active"><a href="#saturday" aria-controls="saturday" role="tab" data-toggle="tab">Saturday 23</a></li>
        <li role="presentation"><a href="#sunday" aria-controls="sunday" role="tab" data-toggle="tab">Sunday 24</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade" id="friday">
            <div class="event">
              <h4 class="event-time">09:30 - 18:30</h4>
              <div class="event-description">
                <h3>Community Bonding Day: Surprise!</h3>
                <p>Pokecone: <b>Codec Wars !</b><br/>
		The VideoLAN organization will pay for whatever costs associated with the event.<br/>
                Discover your team members in a fun game around Tokyo.<br/>
                Please meet in the sponsored hotel lobby (<a href="https://wiki.videolan.org/VDD19/#Accommodation_.2F_Hotel">Villa Fontaine Nihombashi Hakozaki</a>) at 9:30am.
              </div>
            </div>
            <div class="event">
              <h4 class="event-time">19:30</h4>
              <div class="event-description">
                <h3>Evening drinks</h3>
                <p>On <strong>Friday at 19h30</strong>, people are welcome to come and
                share a few good drinks, with all attendees. <!--, at the <a href="https://goo.gl/maps/tTTAPwjzHe62" target="_blank">King George pub</a>. --></p>
                <p>Alcoholic and non-alcoholic drinks will be available!</b>
              </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade active in" id="saturday">
            <div class="event event-breakfast">
              <h4 class="event-time">
                08:30 - 09:00
              </h4>
              <div class="event-description">
                <h3>Registration</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                09:00 - 09:10
              </h4>
              <p class="event-author">
                <span class="avatar avatar-jb"></span>Jean-Baptiste Kempf, VideoLAN
              </p>
              <div class="event-description">
                <h3>Welcome words</h3>
              </div>
            </div>


            <div class="event event-breakfast">
              <h4 class="event-time">
                10:50 - 11:05
              </h4>
              <div class="event-description">
                <h3>Break</h3>
              </div>
            </div>

            <div class="event event-talk">
              <h4 class="event-time">
                12:05 - 12:20
              </h4>
              <div class="event-description">
                <h3>Tokyo Video Tech</h3>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                12:30 - 13:30
              </h4>
              <div class="event-description">
                <h3>Lunch Break</h3>
                <p>Bentō sponsored by <a href="https://www.stream.co.jp/english/" target="_blank">J-Stream</a></p>
              </div>
            </div>

            <div class="event event-lunch">
              <h4 class="event-time">
                13:30 - 14:00
              </h4>
              <div class="event-description">
                <h3>Group Photo</h3>
              </div>
            </div>

            <div class="event event-meetups">
              <h4 class="event-time">
                14:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Meetups (<a href="https://wiki.videolan.org/VDD19/#Planning">See wiki</a>)</h3>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                19:30 - 21:30
              </h4>
              <div class="event-description">
                <h3>Community Dinner</h3>
                <p>Location: Warayakiya, Ginza<br/>You need to be on time.</p>
              </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="sunday">

            <div class="event event-talk">
              <h4 class="event-time">
                09:30 - 12:00
              </h4>
              <div class="event-description">
                <h3>Lightning talks (<a href="https://wiki.videolan.org/VDD19/#Planning">see wiki</a>)</h3>
                <ul>
                </ul>
              </div>
            </div>

            <div class="event event-bg event-lunch">
              <h4 class="event-time">
                12:00 - 14:00
              </h4>
              <div class="event-description">
                <h3>Lunch</h3>
                <p>Bentō sponsored by <a href="https://abema.tv/" target="_blank">AbemaTV</a></p>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                14:00 - 18:00
              </h4>
              <div class="event-description">
                <h3>Unconferences (<a href="https://wiki.videolan.org/VDD19/#Planning">see wiki</a>)</h3>
              </div>
            </div>

            <div class="event">
              <div class="event-inner">
                <div class="event-description">
                    <p>The actual content of the unconference track is being decided on Saturday evening. For the live schedule, check the <a href="https://wiki.videolan.org/VDD19#Unconference_schedule">designated page on the wiki</a>.</p>
                </div>
              </div>
            </div>

            <div class="event">
              <h4 class="event-time">
                19:30 - 21:30
              </h4>
              <div class="event-description">
                <h3>Unofficial Dinner</h3>
                <p>Karaoke at J-House amusement bar</p>
              </div>
            </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Sponsors</h2>
    <hr class="spacer-2">

    <section class="text-center">
     <div class="row col-xs-10 col-xs-offset-1">
	     <div class="col col-md-6 col-sm-12">
        	<a href="https://videolabs.io/" target="_blank">
	          <?php image( 'events/vdd19/sponsors/videolabs.png' , 'Videolabs', 'sponsors-logo-2'); ?>
	        </a>
	      </div>
     </div>
    </section>
  </div>
</section>

<section>
  <div class="container">
    <h2 class="text-center uppercase">Access</h2>
    <hr class="spacer-2">
    <div class="row">
      <div class="col-md-6">
			<p>Trinity College, Dublin</p>
      </div>
      <div class="col-md-6">
			<iframe width="100%" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=-6.260790824890138%2C53.341078409602595%2C-6.250813007354736%2C53.34638196066375&amp;layer=mapnik" style="border: none;"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=17/53.34373/-6.25580">Open map in full</a></small>
      </div>
    </div>

  </div>
</section>


<section>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2 class="uppercase">Code of Conduct </h2>
        <p>This community activity is running under the <a href="https://wiki.videolan.org/CoC/">VideoLAN Code of Conduct</a>. We expect all attendees to respect our <a href="https://wiki.videolan.org/VideoLAN_Values/">Shared Values</a>.</p>
      </div>
      <div class="col-md-6">
        <h2 class="uppercase">Contact </h2>
        <p>The VideoLAN Dev Days are organized by the board members of the VideoLAN non-profit organization, Jean-Baptiste Kempf, Denis Charmet, Felix Paul Khüne and Konstantin Pavlov. You can reach us at <span style="color: #39b549">board@videolan.org</span>.</p>
      </div>
    </div>
  </div>
</section>

<script>
//  $('#videodevdays').html("Vide<span style=\"color:#ff0000;font-weight:bold;\">&#9679;</span> Dev Days 2023");
</script>

<?php footer('$Id: index.php 5400 2009-07-19 15:37:21Z jb $'); ?>
