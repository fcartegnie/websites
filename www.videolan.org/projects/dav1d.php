<?php
   $title = "dav1d - dav1d is an AV1 decoder";
   $lang = "en";
   $new_design = true;
   $menu = array( "projects", "projects" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>

<div class="container">
    <div class="row" style="padding-bottom: 1em">
        <div class="column col-md-4 col-sm-4 hidden-xs">
            <img style="width:100%" src="//images.videolan.org/images/dav1d_logo450x.png" alt="Dav1d"/>
        </div>
        <div class="column col-md-8 col-sm-8 col-xs-12">
            <h1 class="bigtitle">dav1d</h1>
            <div class="projectDescription">
                 <p>dav1d is a new AV1 cross-platform decoder, open-source,
                 and focused on speed, size and correctness.</p>
            </div>
        </div>
    </div>
    <h2 id="about">About</h2>
    <p>dav1d is a new open-source AV1 decoder developed by the VideoLAN and FFmpeg
        communities and sponsored by the <a href="https://aomedia.org">Alliance for
        Open Media</a>.
    </p>
    <h2 id="goals">Goals</h2>
    dav1d aims to be
    <ul class="bullets">
        <li>as fast as possible,</li>
        <li>small,</li>
        <li>very cross-platform.</li>
        <li>correctly threaded</li>
    </ul>
    <h2 id="tech">Technical details</h2>
    <ul class="bullets">
        <li>Uses Meson and Ninja to build</li>
        <li>Written in C99</li>
        <li>Runs on Windows, Linux, macOS and Android</li>
        <li>Licensed under BSD 2-clause "Simplified" License</li>
        <li>As of October 2019, Dav1d is <a target=_blank href="https://medium.com/@ewoutterhoeven/av1-is-ready-for-prime-time-part-2-decoding-performance-d3428221313">the fastest AV1 software decoder</a></li>
    </ul>
    <h2 id="contributing">Contributing</h2>
    <p>You can contribute to dav1d by writing C or ASM, additionally app
    integration and testing is wanted.</p>
    <p>To contribute, check out the <a href="https://code.videolan.org/videolan/dav1d/blob/master/CONTRIBUTING.md">contribution document</a>.</p>
    <p>Most of the development discussion happens on IRC, in the <code>#dav1d</code>
    channel on Libera Chat.</p>
    <h2 id="code">The Code</h2>
    <p>The code can be found on the <a href="https://code.videolan.org/videolan/dav1d">VideoLAN Gitlab</a>:</p>
    <code><pre>git clone https://code.videolan.org/videolan/dav1d.git</pre></code>
    <h2>Source</h2>
    <p>The source code tarball can be found on our ftp: <a href="https://downloads.videolan.org/pub/videolan/dav1d/">dav1d tarballs</a>.
    </p>
</div>

<?php footer(); ?>
